﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Sorama.Utilities.Network
{
    public class NetworkInterface
    {
        private static readonly IPAddress MulticastAddress = IPAddress.Parse("224.0.0.251");

        private System.Net.NetworkInformation.NetworkInterface _interface;

        public NetworkInterface(System.Net.NetworkInformation.NetworkInterface iface)
        {
            _interface = iface;
        }

        public UdpClient CreateUDPClient()
        {
            // Get interface index
            var properties = _interface.GetIPProperties();
            var ipProps = properties.GetIPv4Properties();
            if (ipProps == null)
                throw new NotSupportedException("Network interface does not support IPv4");

            var client = new UdpClient();
            client.Client.SetSocketOption(
                SocketOptionLevel.IP, 
                SocketOptionName.MulticastInterface, 
                IPAddress.HostToNetworkOrder(ipProps.Index));
            
            client.ExclusiveAddressUse = false;
            client.Client.SetSocketOption(SocketOptionLevel.Socket,
                                          SocketOptionName.ReuseAddress,
                                          true);

            client.Client.Bind(new IPEndPoint(IPAddress.Any, 5353));
            
            var multOpt = new MulticastOption(MulticastAddress, ipProps.Index);
            client.Client.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, multOpt);

            return client;
        }

        public static IEnumerable<NetworkInterface> Enumerate()
        {
            return System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces()
                .Where(iface =>
                {
                    // Filter out interfaces that do not support multicasting                    
                    var properties = iface.GetIPProperties();
                    if (properties == null)
                        return false;

                    if (properties.MulticastAddresses?.Any() != true)
                        return false; // most of VPN adapters will be skipped

                    if (!iface.SupportsMulticast)
                        return false; // multicast is meaningless for this type of connection

                    if (iface.OperationalStatus != OperationalStatus.Up)
                        return false; // this adapter is off or not connected

                    var p = properties.GetIPv4Properties();
                    if (null == p)
                        return false; // IPv4 is not configured on this adapter

                    return true;
                }).Select(x => new NetworkInterface(x));

        }

    }
}
