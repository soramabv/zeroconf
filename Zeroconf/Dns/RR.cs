using Sorama.Utilities.Dns;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Heijden.DNS
{
	#region RFC info
	/*
	3.2. RR definitions

	3.2.1. Format

	All RRs have the same top level format shown below:

										1  1  1  1  1  1
		  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
		+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
		|                                               |
		/                                               /
		/                      NAME                     /
		|                                               |
		+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
		|                      TYPE                     |
		+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
		|                     CLASS                     |
		+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
		|                      TTL                      |
		|                                               |
		+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
		|                   RDLENGTH                    |
		+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--|
		/                     RDATA                     /
		/                                               /
		+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+


	where:

	NAME            an owner name, i.e., the name of the node to which this
					resource record pertains.

	TYPE            two octets containing one of the RR TYPE codes.

	CLASS           two octets containing one of the RR CLASS codes.

	TTL             a 32 bit signed integer that specifies the time interval
					that the resource record may be cached before the source
					of the information should again be consulted.  Zero
					values are interpreted to mean that the RR can only be
					used for the transaction in progress, and should not be
					cached.  For example, SOA records are always distributed
					with a zero TTL to prohibit caching.  Zero values can
					also be used for extremely volatile data.

	RDLENGTH        an unsigned 16 bit integer that specifies the length in
					octets of the RDATA field.

	RDATA           a variable length string of octets that describes the
					resource.  The format of this information varies
					according to the TYPE and CLASS of the resource record.
	*/
	#endregion

	/// <summary>
	/// Resource Record (rfc1034 3.6.)
	/// </summary>
    [DebuggerDisplay("Name = {NAME} TTL={TTL} Class={Class} Type = {Type} Record={RECORD}")]
    internal class RR
	{
		/// <summary>
		/// The name of the node to which this resource record pertains
		/// </summary>
		public string NAME;

		/// <summary>
		/// Specifies type of resource record
		/// </summary>
		public Type Type;

		/// <summary>
		/// Specifies type class of resource record, mostly IN but can be CS, CH or HS 
		/// </summary>
		public Class Class;

		/// <summary>
		/// Time to live, the time interval that the resource record may be cached
		/// </summary>
		public uint TTL
		{
			get
			{
				return (uint)Math.Max(0, m_TTL - TimeLived);
			}
			set
			{
				m_TTL = value;
			}
		}
		private uint m_TTL;

		/// <summary>
		/// 
		/// </summary>
		public ushort RDLENGTH;

		/// <summary>
		/// One of the Record* classes
		/// </summary>
		public Record RECORD;

		public int TimeLived;

        public RR(RecordReader rr)
        {
            TimeLived = 0;
            NAME = rr.ReadDomainName();
            Type = (Type)rr.ReadUInt16();
            Class = (Class)rr.ReadUInt16();
            TTL = rr.ReadUInt32();
            RDLENGTH = rr.ReadUInt16();
            RECORD = rr.ReadRecord(Type, RDLENGTH);
            RECORD.RR = this;
        }

        public RR(string name, Type rrType, Class rrClass, uint ttl, Record record)
        {
            TimeLived = 0;
            NAME = name;
            Type = rrType;
            Class = rrClass;
            TTL = ttl;
            RECORD = record;
            RECORD.RR = this;
        }

        public override string ToString()
		{
			return string.Format("{0,-32} {1}\t{2}\t{3}\t{4}",
				NAME,
				TTL,
				Class,
				Type,
				RECORD);
		}

        public byte[] GetRecordBytes()
        {
            IList<byte> buffer = WriteName(NAME).ToList();

            Header.GetNetworkOrderBytes((ushort)Type, buffer);
            Header.GetNetworkOrderBytes((ushort)Class, buffer);
            Header.GetNetworkOrderBytes(TTL, buffer);

            var recordWriter = new RecordWriter();
            RECORD.WriteRecord(recordWriter);

            return buffer.Concat(recordWriter.GetBytes()).ToArray();
        }

        private byte[] WriteName(string src)
        {
            if (!src.EndsWith("."))
                src += ".";

            if (src == ".")
                return new byte[1];

            StringBuilder sb = new StringBuilder();
            int intI, intJ, intLen = src.Length;
            sb.Append('\0');
            for (intI = 0, intJ = 0; intI < intLen; intI++, intJ++)
            {
                sb.Append(src[intI]);
                if (src[intI] == '.')
                {
                    sb[intI - intJ] = (char)(intJ & 0xff);
                    intJ = -1;
                }
            }
            sb[sb.Length - 1] = '\0';
            return Encoding.UTF8.GetBytes(sb.ToString());
        }
    }

    internal class AnswerRR : RR
	{
        public AnswerRR(RecordReader br)
            : base(br)
        {
        }

        public AnswerRR(string name, Type rrType, Class rrClass, uint ttl, Record record)
            : base(name, rrType, rrClass, ttl, record)
        {
        }
    }

    internal class AuthorityRR : RR
	{
		public AuthorityRR(RecordReader br)
			: base(br)
		{
		}

        public AuthorityRR(string name, Type rrType, Class rrClass, uint ttl, Record record)
            : base(name, rrType, rrClass, ttl, record)
        {
        }
    }

    internal class AdditionalRR : RR
	{
		public AdditionalRR(RecordReader br)
			: base(br)
		{
		}

        public AdditionalRR(string name, Type rrType, Class rrClass, uint ttl, Record record)
            : base(name, rrType, rrClass, ttl, record)
        {
        }
    }
}
