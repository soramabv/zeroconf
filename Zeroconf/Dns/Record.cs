// Stuff records are made of

using Sorama.Utilities.Dns;

namespace Heijden.DNS
{
    internal abstract class Record
	{
		/// <summary>
		/// The Resource Record this RDATA record belongs to
		/// </summary>
		public RR RR;

        public abstract void WriteRecord(RecordWriter writer);
    }
}
