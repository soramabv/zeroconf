using System;
using Sorama.Utilities.Dns;
/*
3.4.1. A RDATA format

+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
|                    ADDRESS                    |
+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

where:

ADDRESS         A 32 bit Internet address.

Hosts that have multiple Internet addresses will have multiple A
records.
* 
*/

namespace Heijden.DNS
{
    internal class RecordA : Record
	{
        public string Address;
        private byte[] AddressBytes;

        public RecordA(RecordReader rr)
        {
            Address = string.Format("{0}.{1}.{2}.{3}",
                rr.ReadByte(),
                rr.ReadByte(),
                rr.ReadByte(),
                rr.ReadByte());
        }

        public RecordA(byte[] addressBytes)
        {
            if (addressBytes.Length != 4)
                throw new ArgumentException("Not a valid IP Address (should be exactly 4 bytes).", nameof(addressBytes));

            AddressBytes = addressBytes;
            Address = string.Format("{0}.{1}.{2}.{3}",
                addressBytes[0],
                addressBytes[1],
                addressBytes[2],
                addressBytes[3]);
        }

        public override string ToString()
		{
			return Address;
		}

        public override void WriteRecord(RecordWriter writer)
        {
            // Write length of ip adress bytes (should be 4 for ipv4!)
            writer.WriteUint16((ushort)AddressBytes.Length);

            // Werite bytes
            foreach(var b in AddressBytes)
                writer.WriteUint8(b);
        }
    }
}
