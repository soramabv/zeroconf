using System;
using Sorama.Utilities.Dns;

#region Rfc info
/*
2.2 AAAA data format

   A 128 bit IPv6 address is encoded in the data portion of an AAAA
   resource record in network byte order (high-order byte first).
 */
#endregion

namespace Heijden.DNS
{
    internal class RecordAAAA : Record
	{
        public string Address;
        private byte[] AddressBytes;

        public RecordAAAA(RecordReader rr)
		{
            Address = string.Format("{0:x}:{1:x}:{2:x}:{3:x}:{4:x}:{5:x}:{6:x}:{7:x}",
                rr.ReadUInt16(),
                rr.ReadUInt16(),
                rr.ReadUInt16(),
                rr.ReadUInt16(),
                rr.ReadUInt16(),
                rr.ReadUInt16(),
                rr.ReadUInt16(),
                rr.ReadUInt16());
		}

        public RecordAAAA(byte[] addressBytes)
        {
            if (addressBytes.Length != 16)
                throw new ArgumentException("Not a valid IPv6 Address (should be exactly 16 bytes).", nameof(addressBytes));

            AddressBytes = addressBytes;
            Address = string.Format("{0:x}:{1:x}:{2:x}:{3:x}:{4:x}:{5:x}:{6:x}:{7:x}",
                (addressBytes[0] << 8 | addressBytes[1]),
                (addressBytes[2] << 8 | addressBytes[3]),
                (addressBytes[4] << 8 | addressBytes[5]),
                (addressBytes[6] << 8 | addressBytes[7]),
                (addressBytes[8] << 8 | addressBytes[9]),
                (addressBytes[10] << 8 | addressBytes[11]),
                (addressBytes[12] << 8 | addressBytes[13]),
                (addressBytes[14] << 8 | addressBytes[15]));
        }
        public override string ToString()
        {
            return Address;
        }

        public override void WriteRecord(RecordWriter writer)
        {
            // Write length of ip adress bytes (should be 16 for ipv6!)
            writer.WriteUint16((ushort)AddressBytes.Length);

            // Write bytes
            foreach (var b in AddressBytes)
                writer.WriteUint8(b);
        }
    }
}
