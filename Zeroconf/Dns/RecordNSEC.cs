using Sorama.Utilities.Dns;
using System.Linq;
using System.Text;
/*
4.1.  NSEC RDATA Wire Format

The RDATA of the NSEC RR is as shown below:

      1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2 3 3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/                      Next Domain Name                         /
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/                       Type Bit Maps                           /
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Note: I have no idea how to use this... -Maarten
*/
namespace Heijden.DNS
{
    internal class RecordNSEC : Record
	{
        public string NextDomainName;
        public byte[] Bitmaps;

        public RecordNSEC(RecordReader rr)
        {
            // Re-read length
            ushort RDLENGTH = rr.ReadUInt16(-2);

            var startPosition = rr.Position;

            // Read Next Domain Name
            NextDomainName = rr.ReadDomainName();

            var bitmapsLength = RDLENGTH - (rr.Position - startPosition);

            // Read Bitmaps
            Bitmaps = rr.ReadBytes(bitmapsLength);
        }

        public RecordNSEC(string nextDomainName, byte[] bitmaps)
        {
            NextDomainName = nextDomainName;
            Bitmaps = bitmaps;
        }

        public override string ToString()
		{
			return string.Format("not-used");
		}

        public override void WriteRecord(RecordWriter writer)
        {
            var tempWriter = new RecordWriter();
            tempWriter.WriteDomainName(NextDomainName);

            var nextDomainNameBytes = tempWriter.GetBytes();

            // Write length
            writer.WriteUint16((ushort)(nextDomainNameBytes.Length + Bitmaps.Length));

            // Write Next Domain Name
            writer.WriteBytes(nextDomainNameBytes);

            // Write Bitmaps
            foreach (var b in Bitmaps)
                writer.WriteUint8(b);
        }
    }
}
