using System;
using System.Text;
using System.Collections.Generic;
using Sorama.Utilities.Dns;

#region Rfc info
/*
3.3.14. TXT RDATA format

	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	/                   TXT-DATA                    /
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

where:

TXT-DATA        One or more <character-string>s.

TXT RRs are used to hold descriptive text.  The semantics of the text
depends on the domain where it is found.
 * 
*/
#endregion

namespace Heijden.DNS
{
	internal class RecordTXT : Record
	{
		public List<string> TXT;

		private int _length;

		public RecordTXT(RecordReader rr, int Length)
		{
			int pos = rr.Position;
			TXT = new List<string>();
			while (
				((rr.Position - pos) < Length) &&
				(rr.Position < rr.Length)
				)
			{
				TXT.Add(rr.ReadString());
			}

			SetLength();
		}

		public RecordTXT(List<string> txt)
		{
			TXT = txt;
			SetLength();

		}

		private void SetLength()
		{
			_length = 0;
			foreach (var entry in TXT)
			{
				if (entry.Length > 0xFF)
				{
					throw new ArgumentException("Length of TXT entry exeeds 0xFF bytes.", nameof(TXT));
				}

				_length += entry.Length + 1; // + 1 for length byte
			}

			if (_length > 0XFFFF)
			{
				throw new ArgumentException("Total length of all entries including space needed for length indication bytes exeeds 0xFFFF bytes.", nameof(TXT));
			}
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			foreach (string txt in TXT)
				sb.AppendFormat("\"{0}\" ", txt);
			return sb.ToString().TrimEnd();
		}

		public override void WriteRecord(RecordWriter writer)
		{
			writer.WriteUint16((ushort)_length);

			foreach (var entry in TXT)
			{
				writer.WriteString(entry);
			}
		}
	}
}
