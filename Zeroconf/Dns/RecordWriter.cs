﻿using Heijden.DNS;
using System.Collections.Generic;

namespace Sorama.Utilities.Dns
{
    internal class RecordWriter
    {
        private List<byte> _internalBuffer;

        public RecordWriter()
        {
            _internalBuffer = new List<byte>();
        }

        public void Write(Record record)
        {
            record.WriteRecord(this);
        }

        public void WriteDomainName(string name)
        {
            var remName = name;

            while (remName.Length != 0)
            {
                var index = remName.IndexOf('.');
                if (index == -1)
                    index = remName.Length;

                WriteChar((char)index);
                for (var i = 0; i < index; i++)
                {
                    WriteChar(remName[i]);
                }

                if (index != remName.Length)
                    remName = remName.Substring(index + 1);
                else
                    remName = "";
            }

            WriteChar('\0');
        }

        public void WriteString(string val)
        {
            _internalBuffer.Add((byte)val.Length);
            for (var i = 0; i < val.Length; i++)
            {
                _internalBuffer.Add((byte)val[i]);
            }
        }

        public void WriteChar(char val)
        {
            _internalBuffer.Add((byte)val);
        }

        public void WriteBytes(byte[] val)
        {
            _internalBuffer.AddRange(val);
        }

        public void WriteUint16(ushort val)
        {
            _internalBuffer.Add((byte)((val & 0xFF00) >> 8));
            _internalBuffer.Add((byte)(val & 0xFF));
        }

        public void WriteUint8(byte val)
        {
            _internalBuffer.Add(val);
        }

        public byte[] GetBytes()
        {
            return _internalBuffer.ToArray();
        }
    }
}
