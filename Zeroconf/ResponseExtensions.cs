﻿using Heijden.DNS;
using System.Linq;
using System.Net.Sockets;

namespace Sorama.Utilities
{
    internal static class ResponseExtensions
    {
        internal static readonly uint TTL = 4500; // Same TTL as AVAHI

        internal static void AddDNSSD(this Response resp, ServiceInstance serviceInstance)
        {
            // PTR record to indicate we provide a dns-sd implementation for our service
            var ptrRecord = new RecordPTR(serviceInstance.PTRQueryName);
            var ptrAnswer = new AnswerRR(ZeroconfShared.DNSSDServiceInstanceName, Heijden.DNS.Type.PTR, Class.IN, TTL, ptrRecord);
            resp.Answers.Add(ptrAnswer);
        }

        internal static void AddPTR(this Response resp, ServiceInstance serviceInstance)
        {
            // PTR record to indicate the actual service we provide
            var ptrRecord = new RecordPTR(serviceInstance.ServiceInstanceName);
            var ptrAnswer = new AnswerRR(serviceInstance.PTRQueryName, Heijden.DNS.Type.PTR, Class.IN, TTL, ptrRecord);
            resp.Answers.Add(ptrAnswer);
        }

        internal static void AddTXT(this Response resp, ServiceInstance serviceInstance, bool asAdditional = false)
        {
            // TXT record with service metadata
            var txtRecord = new RecordTXT(serviceInstance.Properties.Select(x => x.Key + "=" + x.Value).ToList());
            if (asAdditional)
                resp.Additionals.Add(new AdditionalRR(serviceInstance.ServiceInstanceName, Heijden.DNS.Type.TXT, Class.IN | Class.FLUSH, TTL, txtRecord));
            else
                resp.Answers.Add(new AnswerRR(serviceInstance.ServiceInstanceName, Heijden.DNS.Type.TXT, Class.IN | Class.FLUSH, TTL, txtRecord));
        }

        internal static void AddSRV(this Response resp, ServiceInstance serviceInstance, bool asAdditional = false)
        {
            var hostName = System.Net.Dns.GetHostName();
            var srvName = hostName + ".local.";

            // SRV record to indicate the hostname and port of the service
            var srvRecord = new RecordSRV(0, 0, serviceInstance.Port, srvName);
            if (asAdditional)
                resp.Additionals.Add(new AdditionalRR(serviceInstance.ServiceInstanceName, Heijden.DNS.Type.SRV, Class.IN | Class.FLUSH, TTL, srvRecord));
            else
                resp.Answers.Add(new AnswerRR(serviceInstance.ServiceInstanceName, Heijden.DNS.Type.SRV, Class.IN | Class.FLUSH, TTL, srvRecord));

            // Also add A and AAAA records when sending SRV
            resp.AddA(serviceInstance, hostName, srvName);
            resp.AddAAAA(serviceInstance, hostName, srvName);
        }

        private static void AddA(this Response resp, ServiceInstance serviceInstance, string hostName, string srvName)
        {
            // Add eachip address associated with the host. According to spec, the client will resolve a (working) entry
            foreach (var entry in System.Net.Dns.GetHostAddresses(hostName).Where(x => x.AddressFamily == AddressFamily.InterNetwork))
            {
                // A record to identify the ipv4 ip address of the hostname of the service
                var aRecord = new RecordA(entry.GetAddressBytes());
                var aAdditional = new AdditionalRR(srvName, Heijden.DNS.Type.A, Class.IN | Class.FLUSH, TTL, aRecord);
                resp.Additionals.Add(aAdditional);
            }
        }

        private static void AddAAAA(this Response resp, ServiceInstance serviceInstance, string hostName, string srvName)
        {
            // Add eachip address associated with the host. According to spec, the client will resolve a (working) entry
            foreach (var entry in System.Net.Dns.GetHostAddresses(hostName).Where(x => x.AddressFamily == AddressFamily.InterNetworkV6))
            {
                // AAAA record to identy fhte ipv6 ip address of the hostname of the service
                var aaaaRecord = new RecordAAAA(entry.GetAddressBytes());
                var aaaaAdditional = new AdditionalRR(srvName, Heijden.DNS.Type.AAAA, Class.IN | Class.FLUSH, TTL, aaaaRecord);
                resp.Additionals.Add(aaaaAdditional);
            }
        }

        internal static void AddNSEC(this Response resp, ServiceInstance serviceInstance)
        {
            var hostName = System.Net.Dns.GetHostName();
            var srvName = hostName + ".local.";

            // NSEC record indicating A and AAAA records present for next serviceInstance name srvName
            var nsecRecord = new RecordNSEC(srvName, new byte[] { 0x00, 0x04, 0x40, 0x00, 0x00, 0x08 }); // Bytes captures from Apple Implementation
            var nsecAdditional = new AdditionalRR(srvName, Heijden.DNS.Type.NSEC, Class.IN | Class.FLUSH, TTL, nsecRecord);
            resp.Additionals.Add(nsecAdditional);

            // NSEC record indicating TXT and SRV records present for next serviceInstance name serviceInstance.ServiceFullDomainName
            nsecRecord = new RecordNSEC(serviceInstance.ServiceInstanceName, new byte[] { 0x00, 0x05, 0x00, 0x00, 0x80, 0x00, 0x40 }); // Bytes captures from Apple Implementation
            nsecAdditional = new AdditionalRR(serviceInstance.ServiceInstanceName, Heijden.DNS.Type.NSEC, Class.IN | Class.FLUSH, TTL, nsecRecord);
            resp.Additionals.Add(nsecAdditional);
        }

        /// <summary>
        /// Send to indicate the service has shut down (TTL = 0)
        /// </summary>
        internal static void AddStopPTR(this Response resp, ServiceInstance serviceInstance)
        {
            var ptrRecord = new RecordPTR(serviceInstance.ServiceInstanceName);
            var ptrAnswer = new AnswerRR(serviceInstance.PTRQueryName, Heijden.DNS.Type.PTR, Class.IN, 0, ptrRecord);
            resp.Answers.Add(ptrAnswer);
        }

        internal static void AddAll(this Response resp, ServiceInstance serviceInstance)
        {
            resp.AddPTR(serviceInstance);
            resp.AddTXT(serviceInstance);
            resp.AddSRV(serviceInstance);
            resp.AddNSEC(serviceInstance);
        }
    }
}
