﻿using Heijden.DNS;
using Sorama.Utilities.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Sorama.Utilities
{
    public class ZeroconfResolver
    {
        /// <summary>
        /// Resolve zeroconf hosts in a given domain.
        /// Here for backwards compatibility reasons.
        /// </summary>
        /// <param name="serviceName">Name of the service type to search for according to RFC6763 sections 4.1 and 7 and RFC6335 section 5.1 (example: _myServiceType._tcp)</param>
        /// <param name="timeout">Time to keep scanning</param>
        /// <returns>A list of services hosted on the scanned domain.</returns>
        /// <remarks>By using yield return construction function can be cancelled when the desired service is found</remarks>
        public IEnumerable<ZeroconfService> Resolve(string serviceName, TimeSpan? timeout = null)
        {
            return Resolve(serviceName, timeout, false);
        }

        /// <summary>
        /// Resolve zeroconf hosts in a given domain.
        /// </summary>
        /// <param name="serviceName">Name of the service type to search for according to RFC6763 sections 4.1 and 7 and RFC6335 section 5.1 (example: _myServiceType._tcp)</param>
        /// <param name="timeout">Time to keep scanning</param>
        /// <param name="restoreScatteredProperties">Set to true to concatenate properties that have the form key[idx]=value into a single key=concatenatedvalue property. Default: false</param>
        /// <returns>A list of services hosted on the scanned domain.</returns>
        /// <remarks>By using yield return construction function can be cancelled when the desired service is found</remarks>
        public IEnumerable<ZeroconfService> Resolve(string serviceName, TimeSpan? timeout = null, bool restoreScatteredProperties = false)
        {
            var to = timeout.HasValue ? timeout.Value : new TimeSpan(0, 0, 3);

            // Get all interfaces to broadcast on
            var interfaces = NetworkInterface.Enumerate();

            // Make sure service name is only the service part and does not contain the instance or domain parts if the user supplied them
            serviceName = serviceName.GetServiceName();

            // Create a question, appending .local. to the serviceType
            var request = new Request();
            request.AddQuestion(new Question(serviceName.GetServicePTRQueryName(), QType.PTR, QClass.ANY));
            var payload = request.Data;

            // Create a list of services that have been discovered
            var serviceList = new List<ZeroconfService>();
            var deviceAddedEvent = new AutoResetEvent(false);
            var cancellationTokenSource = new CancellationTokenSource();
            var token = cancellationTokenSource.Token;     

            // Request service with the given domain
            var disoveryTasks = interfaces.Select(async iface =>
            {
                Task receiverTask = null;

                try
                {
                    // Create a udp client for the given interface
                    using (var client = iface.CreateUDPClientIPv4())
                    {
                        receiverTask = SetupReceiver(client, (ep, data) =>
                        {
                            // Parse services from response
                            var response = new Response(data);
                            var services = ResponseToZeroconfServices(response, ep.Address.ToString(), serviceName, restoreScatteredProperties);
                                                        
                            lock(serviceList)
                            {
                                // Add services to list
                                foreach (var service in services)
                                {
                                    if (!serviceList.Any(x => x.Name == service.Name || x.ConnectionInfo == service.ConnectionInfo))
                                    {
                                        serviceList.Add(service);

                                        // Notify listener that we've added an item
                                        deviceAddedEvent.Set();
                                    }
                                }                     
                            }
                        });

                        while (!token.IsCancellationRequested)
                        {
                            // Send payload
                            client.Send(payload, payload.Length, ZeroconfShared.BroadcastEndPointIPv4);

                            try
                            {
                                await Task.Delay(1000, token);
                            }
                            catch (TaskCanceledException)
                            {

                            }
                        }
                    }
                }
                catch
                {

                }

                if (receiverTask != null)
                    await receiverTask;

            }).ToArray();

            // Let the token source cancel after the timeout
            cancellationTokenSource.CancelAfter(to);

            var lastCount = 0;
            while (!token.IsCancellationRequested)
            {
                if (!deviceAddedEvent.WaitOne(to))
                    break;

                // Acquire lock on the service list
                lock(serviceList)
                {
                    // Yield return all services in the list
                    while(serviceList.Count != lastCount)
                    {
                        yield return serviceList[lastCount];
                        lastCount++;
                    }
                }
            }
            
            var totalTask = Task.WhenAll(disoveryTasks);

            try
            {
                // Make sure all tasks have been completed
                totalTask.Wait();
            }
            catch (AggregateException)
            {

            }

            yield break;
        }

        private async Task SetupReceiver(UdpClient client, Action<IPEndPoint, byte[]> onReceive)
        {
            try
            {
                while (true)
                {
                    var response = await client.ReceiveAsync();
                    onReceive(response.RemoteEndPoint, response.Buffer);
                }
            }
            catch (SocketException)
            {
            }
            catch (ObjectDisposedException)
            {
            }
        }

        private IEnumerable<ZeroconfService> ResponseToZeroconfServices(Response response, string remoteAddress, string serviceName, bool restoreScatteredProperties)
        {
            var serviceInstanceNames = new List<string>();
            var services = new List<ZeroconfService>();

            var records = response.Answers.Cast<RR>().Concat(response.Additionals.Cast<RR>())
                .Where(x => x.Type == Heijden.DNS.Type.PTR || x.Type == Heijden.DNS.Type.SRV || x.Type == Heijden.DNS.Type.TXT)
                .ToArray();

            foreach (var record in records.Where(x => x.Type == Heijden.DNS.Type.PTR))
            {
                var ptrRecord = record.RECORD as RecordPTR;
                if (ptrRecord == null)
                {
                    // Invalid entry, ignore this service
                    continue;
                }

                var match = ZeroconfShared.ServiceInstanceNameRegExp.Match(ptrRecord.PTRDNAME);
                if (match.Success && match.Groups["service"].Value == serviceName)
                {
                    serviceInstanceNames.Add(match.Value);
                }
            }

            foreach (var serviceInstanceName in serviceInstanceNames)
            {
                var srvRecord = records.FirstOrDefault(x => x.Type == Heijden.DNS.Type.SRV && x.NAME == serviceInstanceName)?.RECORD as RecordSRV;
                var txtRecord = records.FirstOrDefault(x => x.Type == Heijden.DNS.Type.TXT && x.NAME == serviceInstanceName)?.RECORD as RecordTXT;

                // Only add the service if both SRV and TXT records have been found and are valid
                if (srvRecord != null && txtRecord != null)
                {
                    var port = srvRecord.PORT;
                    var properties = ParseTXT(txtRecord, restoreScatteredProperties);
                    services.Add(new ZeroconfService(serviceInstanceName, remoteAddress + ":" + port, properties));
                }
            }

            return services;
        }

        private Dictionary<string, string> ParseTXT(RecordTXT txtRecord, bool restoreScatteredProperties)
        {
            var properties = new Dictionary<string, string>();

            var scatteredProperties = new Dictionary<string, Dictionary<int, string>>();
            foreach (var entry in txtRecord.TXT)
            {
                var splitIndex = entry.IndexOf('=');
                if (splitIndex == -1)
                {
                    // Invalid TXT entry, ignore
                    continue;
                }

                var key = entry.Substring(0, splitIndex);
                var value = entry.Substring(splitIndex + 1);

                // Regex matches <key>[<idx>] pattern to extract scattered properties crossing the 255 character boundary of TXT record entries
                var match = Regex.Match(key, @"^(?<key>.*)\[(?<idx>[0-9]+)\]$");
                if (restoreScatteredProperties && match.Success)
                {
                    // Property is spread over multiple entries
                    var propertyKey = match.Groups["key"].Value;
                    var index = int.Parse(match.Groups["idx"].Value);

                    if (!scatteredProperties.ContainsKey(propertyKey))
                        scatteredProperties.Add(propertyKey, new Dictionary<int, string>());

                    scatteredProperties[propertyKey].Add(index, value);
                }
                else
                {
                    // Single entry property
                    properties.Add(key, value);
                }
            }

            // Reconstruct scattered properties
            foreach (var scatteredProperty in scatteredProperties)
            {
                var key = scatteredProperty.Key;
                var parts = scatteredProperty.Value;

                properties.Add(key, string.Join("", parts.OrderBy(x => x.Key).Select(part => part.Value)));
            }

            return properties;
        }
    }
}