﻿using Heijden.DNS;
using Sorama.Utilities.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using NetworkInterface = Sorama.Utilities.Network.NetworkInterface;

namespace Sorama.Utilities
{
    public class ZeroconfResponder
    {       
        private Dictionary<Guid, ServiceInstance> _serviceInstances;
        private List<ZeroconfNetworkInterface> _interfaces;

        public ZeroconfResponder()
        {
            _serviceInstances = new Dictionary<Guid, ServiceInstance>();
            _interfaces = new List<ZeroconfNetworkInterface>();
        }

        public async Task<Guid> RegisterService(ServiceInstance serviceInstance)
        {
            return await Task.Factory.StartNew(() =>
            {
                lock (_serviceInstances)
                {
                    if (_serviceInstances.Count == 0)
                    {
                        // First service instance to be registered here, register a network change listener to make sure our client list is up to date
                        NetworkChange.NetworkAddressChanged += NetworkAddressChanged;

                        // Trigger an update ourselves
                        NetworkAddressChanged(this, new EventArgs());
                    }

                    // TODO: Check if the service already exists on the network?
                    
                    var domainId = Guid.NewGuid();
                    _serviceInstances.Add(domainId, serviceInstance);

                    var resp = new Response();
                    resp.AddAll(_serviceInstances[domainId]);
                    var bytes = resp.GetResponseBytes();

                    serviceInstance.StartAnnounce(() =>
                    {
                        lock (_interfaces)
                        {
                            foreach (var iface in _interfaces)
                            {
                                iface.Send(bytes, bytes.Length, ZeroconfShared.BroadcastEndPointIPv4);
                            }
                        }
                    });

                    return domainId;
                }
            });
        }

        public void UnregisterService(Guid domainId)
        {
            lock (_serviceInstances)
            {
                if(_serviceInstances.ContainsKey(domainId))
                {
                    var resp = new Response();
                    resp.AddStopPTR(_serviceInstances[domainId]);
                    var bytes = resp.GetResponseBytes();

                    foreach (var iface in _interfaces)
                    {
                        iface.Send(bytes, bytes.Length, ZeroconfShared.BroadcastEndPointIPv4);
                    }

                    // Stop the service instance from announcing and remove it from the list of instances
                    var serviceInstance = _serviceInstances[domainId];
                    serviceInstance.StopAnnounce();
                    _serviceInstances.Remove(domainId);
                }

                if (_serviceInstances.Count == 0)
                {
                    // Stop listening for network changes
                    NetworkChange.NetworkAddressChanged -= NetworkAddressChanged;

                    // Clear all interfaces we currently have
                    lock (_interfaces)
                    {
                        foreach (var iface in _interfaces)
                        {
                            iface.Stop();
                        }

                        _interfaces.Clear();
                    }
                }
            }
        }

        private void ParseMessageAndSendResponse(byte[] data, ZeroconfNetworkInterface iface)
        {
            var response = new Response(data);
            var header = response.header;

            if (header.QDCOUNT != 0)
            {
                lock (_serviceInstances)
                {
                    var resp = new Response();

                    // Check questions for our domains
                    var questions = response.Questions;
                    foreach (var question in questions)
                    {
                        var serviceQuestionName = question.QName;
                        foreach (var serviceInstance in _serviceInstances.Values)
                        {
                            if (serviceQuestionName == serviceInstance.PTRQueryName)
                            {
                                switch (question.QType)
                                {
                                    case QType.PTR:
                                        resp.AddPTR(serviceInstance);
                                        resp.AddTXT(serviceInstance, true);
                                        resp.AddSRV(serviceInstance, true);
                                        resp.AddNSEC(serviceInstance);
                                        break;
                                    case QType.ANY:
                                    default: // TODO: Find out how to respond to other QTypes properly
                                        resp.AddAll(serviceInstance);
                                        break;
                                }
                            }
                            else if (serviceQuestionName == serviceInstance.ServiceInstanceName)
                            {
                                switch (question.QType)
                                {
                                    case QType.TXT:
                                        resp.AddTXT(serviceInstance);
                                        break;
                                    case QType.SRV:
                                        resp.AddSRV(serviceInstance);
                                        break;
                                    case QType.ANY:
                                        resp.AddAll(serviceInstance);
                                        break;
                                }
                            }
                            else if (serviceQuestionName == ZeroconfShared.DNSSDServiceInstanceName)
                            {
                                // RFC6763 Section 9: A DNS query for PTR records with the name "_services._dns-sd._udp.<Domain>" yields a set of PTR records,
                                // where the rdata of each PTR record is the two-label <Service> name, plus the same domain, e.g., "_http._tcp.<Domain>".
                                resp.AddDNSSD(serviceInstance);
                            }
                        }
                    }

                    var bytes = resp.GetResponseBytes();
                    iface.Send(bytes, bytes.Length, ZeroconfShared.BroadcastEndPointIPv4);
                }
            }
        }

        private void NetworkAddressChanged(object sender, EventArgs e)
        {
            lock (_interfaces)
            {
                var interfaces = NetworkInterface.Enumerate().ToList();

                var newInterfaces = interfaces.Where(x => _interfaces.All(y => !y.Is(x))).ToList();
                var oldInterfaces = _interfaces.Where(x => interfaces.All(y => !x.Is(y))).ToList();

                // Remove all old interfaces
                foreach (var oldInterface in oldInterfaces)
                {
                    oldInterface.Stop();
                    _interfaces.Remove(oldInterface);
                }

                // Add all new interfaces
                foreach (var newInterface in newInterfaces)
                {
                    var iface = new ZeroconfNetworkInterface(newInterface);
                    _interfaces.Add(iface);

                    iface.Start(data => ParseMessageAndSendResponse(data, iface));
                }
            }
        }

        private class ZeroconfNetworkInterface
        {
            private readonly NetworkInterface _networkInterface;
            private UdpClient _client;
            private Task _receiveTask;
            private CancellationTokenSource _tokenSource;

            public ZeroconfNetworkInterface(NetworkInterface networkInterface)
            {
                _networkInterface = networkInterface;
            }

            public bool Is(NetworkInterface iface)
            {
                return Equals(_networkInterface, iface);
            }

            public async void Start(Action<byte[]> onReceive)
            {
                _client = _networkInterface.CreateUDPClientIPv4();
                _tokenSource = new CancellationTokenSource();

                try
                {
                    while (!_tokenSource.Token.IsCancellationRequested)
                    {
                        var result = await _client.ReceiveAsync();
                        onReceive(result.Buffer);
                    }
                }
                catch (SocketException)
                {
                }
                catch (ObjectDisposedException)
                {
                }

                _client.Close();
            }

            public void Send(byte[] bytes, int byteLength, IPEndPoint endPoint)
            {
                _client.Send(bytes, byteLength, endPoint);
            }

            public void Stop()
            {
                _tokenSource.Cancel();
                _client.Close();
            }
        }
    }
}