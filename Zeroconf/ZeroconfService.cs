﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Sorama.Utilities
{
    public class ZeroconfService
    {
        public string Name { get; private set; }

        public string ConnectionInfo { get; private set; }

        public IReadOnlyDictionary<string,string> Properties { get; private set; }

        internal ZeroconfService(string name, string connectionInfo, IDictionary<string,string> properties)
        {
            Name = name;
            ConnectionInfo = connectionInfo;
            Properties = properties is IReadOnlyDictionary<string, string> ? (IReadOnlyDictionary<string, string>)properties :
                new ReadOnlyDictionary<string, string>(properties);
        }
    }
}
