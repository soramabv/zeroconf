﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Sorama.Utilities
{
    public class ServiceInstance
    {
        public string Instance { get; }

        public string Service { get; }

        public string PTRQueryName
        {
            get
            {
                // RFC6763 Seciton 7.2: Name used for PTR queries: <sn>._tcp.<servicedomain>.<parentdomain>. where <servicedomain>.<parentdomain>. = local.
                return Service + ".local.";
            }
        }

        public string ServiceInstanceName
        {
            get
            {
                // RFC6763: Service Instance Name = <Instance> . <Service> . <Domain>
                return Instance + "." + Service + ".local.";
            }
        }

        public ushort Port { get; }

        public Dictionary<string, string> Properties { get; }

        /// <summary>
        /// Creates a new Service Instance for publishing
        /// </summary>
        /// <param name="serviceInstanceName">The full service instance name according to RFC6763 Section 4.1. 
        /// The .local. part maybe ommited since it is implied for Zeroconf services</param>
        /// <param name="port">The port on which the service is hosted</param>
        /// <param name="properties">Additional properties to optionally add when publishing the service. 
        /// The total amount of bytes to represent all properties (including the keys and = characters) 
        /// should not exeed 0xFFFF and should idealy be less then 512 bytes (See RFC6891 Section 4.3).</param>
        /// <param name="scatterLongProperties">Set to true to scatter properties which are to long 
        /// (Key.Length + Value.Length + 1 (for the = ) > 0xFF according to standard DNS TXT record spec RFC6763 Section 6.1.)
        /// over multiple properties using the notation: key[idx]. Default: false 
        /// (truncates properties that are too long, which is the default for the Apple Bonjour implementation)</param>
        public ServiceInstance(string serviceInstanceName, ushort port = default(ushort), Dictionary<string, string> properties = default(Dictionary<string, string>), bool scatterLongProperties = false)
        {
            var match = ZeroconfShared.ServiceInstanceNameRegExp.Match(serviceInstanceName);

            if (!match.Success)
                throw new ArgumentException("No valid Service Instance Name provided. See RFC6763 sections 4.1 and 7 and RFC6335 section 5.1 for more information.", nameof(serviceInstanceName));

            var instance = match.Groups["instance"].Value;
            var service = match.Groups["service"].Value;

            Instance = instance;
            Service = service;
            Port = port;

            // Add properties one by one, checking if they exeed the limit of 255 characters (including key name and =) and if they do, either scatter (scatterLongProperties = true) or truncate them.
            // Truncation is done by default by the Apple Bonjour implementation (though it caps the length of the TXT record entrys to 0xF1 instead of 0xFF, there is no reference to this in any of the standards).
            // Scattering is our own custom implementation. It divides the strings that are too long for a single entry over multiple entries and adds [index] to
            // the key of the property. For example, if you have a property with key "myproperty" with a value of length 260 characters, the TXT record entries look like:
            // myproperty[0]=<character 0 trough 240>
            // myproperty[1]=<character 241 trough 259>
            Properties = new Dictionary<string, string>();
            foreach (var property in properties)
            {
                var maxPropertyValueLength = 0xFF - (property.Key.Length + 1); // + 1 for the = automatically added to TXT record entries
                if (property.Value.Length > maxPropertyValueLength) 
                {
                    if (scatterLongProperties)
                    {
                        // Property is too long to fit in a single TXT entry, scatter it
                        var key = property.Key;
                        var remainingChars = property.Value.ToCharArray();
                        for (var idx = 0; remainingChars.Length > 0; idx++)
                        {
                            var indexedKey = key + "[" + idx + "]";
                            var nrOfChars = Math.Min(remainingChars.Length, 0xFF - (indexedKey.Length + 1)); // + 1 for the = automatically added to TXT record entries
                            var s = new string(remainingChars.Take(nrOfChars).ToArray());
                            remainingChars = remainingChars.Skip(nrOfChars).ToArray();

                            Properties.Add(indexedKey, s);
                        }
                    }
                    else
                    {
                        // Truncate property
                        Properties.Add(property.Key, new string(property.Value.Take(maxPropertyValueLength).ToArray()));
                    }
                }
                else
                {
                    // Single entry property
                    Properties.Add(property.Key, property.Value);
                }
            }
        }

        #region Announce Helper
        private const int ReannounceMaxMs = 60 * 60 * 1000;
        private const int ReannounceFactor = 3;
        private const int SleepCancelIntervalMs = 100;
        private int _announceTimeout = 1000;        
        private CancellationTokenSource _tokenSource;

        public void StartAnnounce(Action AnnounceAction)
        {
            _tokenSource = new CancellationTokenSource();

            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    AnnounceAction();

                    var startTime = DateTime.UtcNow;
                    while (DateTime.UtcNow - startTime < TimeSpan.FromMilliseconds(_announceTimeout) && !_tokenSource.Token.IsCancellationRequested)
                    {
                        Thread.Sleep(SleepCancelIntervalMs);
                    }

                    _announceTimeout = Math.Min(_announceTimeout*ReannounceFactor, ReannounceMaxMs);

                    if (_tokenSource.Token.IsCancellationRequested)
                    {
                        _tokenSource = null;
                        return;
                    }
                }
            });
        }

        public void StopAnnounce()
        {
            _tokenSource?.Cancel();
        }
        #endregion
    };

    internal static class ZeroconfShared
    {
        // For more information on these ports see: http://grouper.ieee.org/groups/1722/contributions/2009/Bonjour%20Device%20Discovery.pdf and http://www.ietf.org/rfc/rfc6762.txt
        internal static readonly IPEndPoint BroadcastEndPointIPv4 = new IPEndPoint(IPAddress.Parse("224.0.0.251"), 5353);
        internal static readonly IPEndPoint BroadcastEndPointIPv6 = new IPEndPoint(IPAddress.Parse("ff02::fb"), 5353);

        // RFC6763 Section 9: A DNS query for PTR records with the name "_services._dns-sd._udp.<Domain>" yields a set of PTR records,
        // where the rdata of each PTR record is the two-label <Service> name, plus the same domain, e.g., "_http._tcp.<Domain>".
        internal static readonly string DNSSDServiceInstanceName = "_services._dns-sd._udp.local.";

        // Matches all valid ServiceInstanceNames according to RFC6763 sections 4.1 and 7, <service> part according to RFC6335 section 5.1
        internal static readonly Regex ServiceInstanceNameRegExp = new Regex(@"^(?<instance>[^\u0000-\u001F\u007F]{0,63}?).?(?<service>(?:_[A-Za-z]{1}(?:[A-Za-z0-9]|(?:-(?=[A-Za-z0-9]))){0,14})._(?:tcp|udp)).?(?<domain>local)?.?$");

        internal static string GetServiceName(this string serviceInstanceName)
        {
            var match = ServiceInstanceNameRegExp.Match(serviceInstanceName);

            if (!match.Success)
                throw new ArgumentException("No valid Service Instance Name provided. See RFC6763 sections 4.1 and 7 and RFC6335 section 5.1 for more information.", nameof(serviceInstanceName));

            var service = match.Groups["service"].Value;

            return service;
        }

        internal static string GetServicePTRQueryName(this string serviceInstanceName)
        {
            var match = ServiceInstanceNameRegExp.Match(serviceInstanceName);

            if (!match.Success)
                throw new ArgumentException("No valid Service Instance Name provided. See RFC6763 sections 4.1 and 7 and RFC6335 section 5.1 for more information.", nameof(serviceInstanceName));

            var service = match.Groups["service"].Value;

            var domainMatch = match.Groups["domain"].Value;
            var domain = string.IsNullOrWhiteSpace(domainMatch) ? "local" : domainMatch;

            return service + "." + domain + ".";
        }
    }
}
