﻿using System;
using System.Windows;
using Bonjour;
using Sorama.Utilities;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ZeroconfTest.NetFx
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Own Implementation
        private ZeroconfResolver ownResolver = new ZeroconfResolver();
        private ZeroconfResponder ownResponder = new ZeroconfResponder();
        private Guid serviceId, serviceId2;

        // Apple Bonjour Reference Implementation
        // Requires Bonjour SDK to be installed: https://developer.apple.com/bonjour/
        private DNSSDService service;
        private DNSSDEventManager eventManager;

        public MainWindow()
        {
            InitializeComponent();

            service = new DNSSDService();
            eventManager = new DNSSDEventManager();

            eventManager.ServiceFound += EventManager_ServiceFound;
            eventManager.ServiceLost += EventManager_ServiceLost;
            eventManager.ServiceResolved += EventManager_ServiceResolved;
            eventManager.ServiceRegistered += EventManager_ServiceRegistered;
            eventManager.AddressFound += EventManager_AddressFound;
            eventManager.DomainFound += EventManager_DomainFound;
            eventManager.DomainLost += EventManager_DomainLost;
            eventManager.MappingCreated += EventManager_MappingCreated;
            eventManager.QueryRecordAnswered += EventManager_QueryRecordAnswered;
            eventManager.RecordRegistered += EventManager_RecordRegistered;
            eventManager.OperationFailed += EventManager_OperationFailed;
        }

        private void EventManager_ServiceFound(DNSSDService browser, DNSSDFlags flags, uint ifIndex, string serviceName, string regtype, string domain)
        {
            Console.WriteLine("EventManager_ServiceFound: " + serviceName + " regtype: " + regtype + " domain: " + domain);
            browser.Resolve(0, ifIndex, serviceName, regtype, domain, eventManager);
        }

        private void EventManager_ServiceLost(DNSSDService browser, DNSSDFlags flags, uint ifIndex, string serviceName, string regtype, string domain)
        {
            Console.WriteLine("EventManager_ServiceLost: " + serviceName + " regtype: " + regtype + " domain: " + domain);
        }
        private void EventManager_ServiceResolved(DNSSDService service, DNSSDFlags flags, uint ifIndex, string fullname, string hostname, ushort port, TXTRecord record)
        {
            Console.WriteLine("EventManager_ServiceResolved: " + fullname);
            for(uint i = 0; i < record.GetCount(); i++)
            {
                var key = record.GetKeyAtIndex(i);
                var value = Encoding.UTF8.GetString(record.GetValueForKey(key));

                Console.WriteLine(key + " = " + value);
            }
        }

        private void EventManager_ServiceRegistered(DNSSDService service, DNSSDFlags flags, string name, string regtype, string domain)
        {
            Console.WriteLine("EventManager_ServiceRegistered: " + name + " regtype: " + regtype + " domain: " + domain);
        }

        private void EventManager_AddressFound(DNSSDService service, DNSSDFlags flags, uint ifIndex, string hostname, DNSSDAddressFamily addressFamily, string address, uint ttl)
        {
            Console.WriteLine("EventManager_AddressFound");
        }

        private void EventManager_DomainFound(DNSSDService service, DNSSDFlags flags, uint ifIndex, string domain)
        {
            Console.WriteLine("EventManager_DomainFound");
        }

        private void EventManager_DomainLost(DNSSDService service, DNSSDFlags flags, uint ifIndex, string domain)
        {
            Console.WriteLine("EventManager_DomainLost");
        }

        private void EventManager_MappingCreated(DNSSDService service, DNSSDFlags flags, uint ifIndex, uint externalAddress, DNSSDAddressFamily addressFamily, DNSSDProtocol protocol, ushort internalPort, ushort externalPort, uint ttl)
        {
            Console.WriteLine("EventManager_MappingCreated");
        }

        private void EventManager_QueryRecordAnswered(DNSSDService service, DNSSDFlags flags, uint ifIndex, string fullname, DNSSDRRType rrtype, DNSSDRRClass rrclass, object rdata, uint ttl)
        {
            Console.WriteLine("EventManager_QueryRecordAnswered");
        }

        private void EventManager_RecordRegistered(DNSSDRecord record, DNSSDFlags flags)
        {
            Console.WriteLine("EventManager_RecordRegistered" );
        }

        private void EventManager_OperationFailed(DNSSDService service, DNSSDError error)
        {
            Console.WriteLine("EventManager_OperationFailed: " + error);
        }

        private byte[] testBytes = Encoding.UTF8.GetBytes("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi suscipit vestibulum justo, nec interdum ante blandit ac. Suspendisse dui tortor, pretium posuere est iaculis, auctor dictum ipsum. Maecenas auctor viverra tempus. Aliquam dapibus tempus felis. Donec vestibulum ex eget dui elementum dictum. Maecenas mattis urna sit amet molestie imperdiet. Fusce gravida, lacus at volutpat porttitor, quam arcu tempor orci, ac finibus erat elit et erat. Aenean aliquam condimentum hendrerit. Duis porttitor tempus enim, ac sollicitudin sapien malesuada ut. Etiam aliquam convallis nibh, id mattis augue sodales vel. Maecenas lectus massa, tincidunt nec porttitor id, venenatis sollicitudin nibh. Proin ac laoreet libero. Nam vitae lorem ut velit efficitur maximus nec ac tortor. Fusce varius sollicitudin cursus. Vivamus ut nunc mauris. Nam at fermentum eros, nec porttitor lacus. Vivamus tincidunt aliquet ex. Etiam in diam tempus ante suscipit accumsan at in nisl. Mauris commodo faucibus erat, sit sed.");
        private void OwnButton_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(async() =>
            {
                var properties = new Dictionary<string, string>
                {
                    { "foo", "bar" },
                    { "test", "5080" },
                    { "implementation", "sorama" },
                    { "longproperty", Encoding.UTF8.GetString(testBytes) }
                };

                var service = new ServiceInstance("myawesomeservice._sorama._tcp", 1337, properties, true);
                var service2 = new ServiceInstance("Maarten is awesome._sorama._tcp", 13347, properties);

                serviceId = await ownResponder.RegisterService(service);
                serviceId2 = await ownResponder.RegisterService(service2);

                Console.WriteLine("Service Registered: " + service.ServiceInstanceName + " with Id: " + serviceId);
                Console.WriteLine("Service Registered: " + service2.ServiceInstanceName + " with Id: " + serviceId2);
            });
        }

        private void ResolveOwnButton_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    foreach (var service in ownResolver.Resolve("_sorama._tcp", null, true))
                    {
                        Console.WriteLine("Service Found: " + service.Name);
                        foreach(var entry in service.Properties)
                        {
                            Console.WriteLine(entry.Key + " = " + entry.Value);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exeption: " + ex.Message);
                }
            });
        }

        private void StopOwnButton_Click(object sender, RoutedEventArgs e)
        {
            ownResponder.UnregisterService(serviceId);
            ownResponder.UnregisterService(serviceId2);

            Console.WriteLine("Services Unregistered");
        }


        private void AppleButton_Click(object sender, RoutedEventArgs e)
        {
            var txtRecord = new TXTRecord();
            txtRecord.SetValue("foo", "bar");
            txtRecord.SetValue("test", "5080");
            txtRecord.SetValue("implementation", "apple");
            txtRecord.SetValue("longproperty", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi suscipit vestibulum justo, nec interdum ante blandit ac. Suspendisse dui tortor, pretium posuere est iaculis, auctor dictum ipsum. Maecenas auctor viverra tempus. Aliquam dapibus tempus felis. Donec vestibulum ex eget dui elementum dictum. Maecenas mattis urna sit amet molestie imperdiet. Fusce gravida, lacus at volutpat porttitor, quam arcu tempor orci, ac finibus erat elit et erat. Aenean aliquam condimentum hendrerit. Duis porttitor tempus enim, ac sollicitudin sapien malesuada ut. Etiam aliquam convallis nibh, id mattis augue sodales vel. Maecenas lectus massa, tincidunt nec porttitor id, venenatis sollicitudin nibh. Proin ac laoreet libero. Nam vitae lorem ut velit efficitur maximus nec ac tortor. Fusce varius sollicitudin cursus. Vivamus ut nunc mauris. Nam at fermentum eros, nec porttitor lacus. Vivamus tincidunt aliquet ex. Etiam in diam tempus ante suscipit accumsan at in nisl. Mauris commodo faucibus erat, sit sed.");

            try
            {
                service.Register(0, 0, "myawesomeservice", "_sorama._tcp.", null, null, 1337, txtRecord, eventManager);
                service.Register(0, 0, "Maarten is awesome", "_sorama._tcp.", null, null, 13347, txtRecord, eventManager);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption: " + ex.Message);
            }
        }

        private void ResolveAppleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                service.Browse(0, 0, "_sorama._tcp.", null, eventManager);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption: " + ex.Message);
            }
        }

        private void StopAppleButton_Click(object sender, RoutedEventArgs e)
        {
            service.Stop();
            service = new DNSSDService();
        }
    }
}
